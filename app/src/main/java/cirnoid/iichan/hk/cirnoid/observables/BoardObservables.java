package cirnoid.iichan.hk.cirnoid.observables;


import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kerrigan on 20.04.15.
 */
public class BoardObservables {
    public static Observable<String> getBoard(final OkHttpClient http){
        return Observable.create(new rx.Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {

                Request request = new Request.Builder().url("http://iichan.hk/b/").get().build();
                Response response = null;
                try {
                    response = http.newCall(request).execute();
                    String result = response.body().string();
                    subscriber.onNext(result);

                } catch (IOException e) {
                    subscriber.onError(e);
                }
                subscriber.onCompleted();

            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
