package cirnoid.iichan.hk.cirnoid;

import android.app.Application;

import java.util.Arrays;

import cirnoid.iichan.hk.cirnoid.modules.ActivityModule;
import cirnoid.iichan.hk.cirnoid.modules.HttpModule;
import dagger.ObjectGraph;

/**
 * Created by kerrigan on 20.04.15.
 */
public class CirnoidApplication extends Application {
    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        objectGraph = ObjectGraph.create(getModules());
    }

    public void inject(Object target){
        objectGraph.inject(target);
    }

    private Object[] getModules() {
        return Arrays.asList(new HttpModule(), new ActivityModule()).toArray();
    }


}
