package cirnoid.iichan.hk.cirnoid.modules;

import android.app.Fragment;

import java.util.HashMap;
import java.util.Map;

import cirnoid.iichan.hk.cirnoid.activities.MainActivity;
import cirnoid.iichan.hk.cirnoid.fragments.BoardsFragment;
import dagger.Module;
import dagger.Provides;

/**
 * Created by kerrigan on 23.04.15.
 */
@Module(
        injects = MainActivity.class
)
public class ActivityModule {
    @Provides
    Map<String, Fragment> provideFragmentsContainer(){
        return new HashMap<>();
    }

    @Provides
    BoardsFragment provideBoardsFragment(){
        return new BoardsFragment();
    }

}
