package cirnoid.iichan.hk.cirnoid.modules;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import cirnoid.iichan.hk.cirnoid.fragments.BoardsFragment;
import dagger.Module;
import dagger.Provides;

/**
 * Created by kerrigan on 20.04.15.
 */
@Module(
        injects = BoardsFragment.class
)
public class HttpModule {

    @Provides @Singleton
    OkHttpClient provideHttp(){
        return new OkHttpClient();
    }
}
