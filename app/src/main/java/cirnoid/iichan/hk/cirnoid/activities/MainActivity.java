package cirnoid.iichan.hk.cirnoid.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Map;

import javax.inject.Inject;

import cirnoid.iichan.hk.cirnoid.CirnoidApplication;
import cirnoid.iichan.hk.cirnoid.R;
import cirnoid.iichan.hk.cirnoid.fragments.BoardsFragment;


public class MainActivity extends ActionBarActivity {
    @Inject
    Map<String, Fragment> boards;
    @Inject
    Map<String, Fragment> threads;
    @Inject
    BoardsFragment boardsListFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((CirnoidApplication)getApplication()).inject(this);
        setContentView(R.layout.activity_main);

        boardsListFragment = new BoardsFragment();

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, boardsListFragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
