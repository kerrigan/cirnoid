package cirnoid.iichan.hk.cirnoid.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import cirnoid.iichan.hk.cirnoid.CirnoidApplication;
import cirnoid.iichan.hk.cirnoid.R;
import cirnoid.iichan.hk.cirnoid.observables.BoardObservables;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by kerrigan on 06.04.15.
 */
public class BoardsFragment extends Fragment {
    private static final String TAG = BoardsFragment.class.getSimpleName();
    @Inject OkHttpClient http;

    @InjectView(R.id.lstBoards)
    RecyclerView lstBoards;
    private RecyclerView.Adapter mAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((CirnoidApplication)getActivity().getApplication()).inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_boardlist, null);
        ButterKnife.inject(this, v);

        lstBoards.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        //lstBoards.setAdapter(mAdapter);


        BoardObservables.getBoard(http).subscribe((s) -> {
                    //TODO: here success
                    Log.i(TAG, s);
                },
                (e) -> {
                    //TODO: here error
                    Log.e(TAG, e.toString());
                });
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
