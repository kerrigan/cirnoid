package cirnoid.iichan.hk.cirnoid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import cirnoid.iichan.hk.cirnoid.activities.SecretActivity;

public class SecretReceiver extends BroadcastReceiver {
    public SecretReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Intent newIntent = new Intent(context, SecretActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(newIntent);
    }
}
